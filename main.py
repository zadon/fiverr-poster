from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from peewee import *
import datetime
from multiprocessing import Process, Pool, Queue
import random


accounts = open('configurations/accounts.txt', 'r').readlines()
messages = open('configurations/messages.txt', 'r').readlines()
proxies = open('configurations/proxies.txt', 'r').readlines()
target_urls = open('configurations/target_urls.txt', 'r').readlines()

db = SqliteDatabase('main.db')


class Message(Model):
    text = TextField(unique=True)

    class Meta:
        database = db


class Freelancer(Model):
    freelancer_name = CharField(unique=True)
    last_message_posted_time = DateTimeField(null=True)
    message = ForeignKeyField(Message, null=True)
    gig_id = IntegerField(null=True)

    class Meta:
        database = db

db.connect()
try:
    db.create_tables([Message, Freelancer])
except Exception:
    pass

target_url = 'https://www.fiverr.com/categories/video-animation/whiteboard-explainer-videos/#filter=rating&page=1&ref=animation_type%3Awhiteboard_2d_black_white'
login_url = 'https://www.fiverr.com/login'

urls_queue = Queue()
for x in target_urls:
    urls_queue.put(x)

accounts_queue = Queue()
for x in accounts:
    accounts_queue.put(x)


class Fiverr(Process):
    def __init__(self, message=None, proxy=None, timeout=10):
        super(Fiverr, self).__init__()
        self.gigs = []
        self.message = message
        self.database_mesage = Message.get_or_create(text=message)[0]
        proxy = proxy.split(':')
        host = proxy[0]
        port = proxy[1]
        self.browser = self.my_proxy(host, port, driver_id)
        self.browser.set_window_size(1920, 1080)
        self.timeout = timeout
        self.page_number = 0
        self.working_url = None
        self.last_page = False

    @staticmethod
    def my_proxy(PROXY_HOST, PROXY_PORT, driver_id):
        if driver_id == 1:
            fp = webdriver.FirefoxProfile()
            fp.set_preference("network.proxy.type", 1)
            fp.set_preference("network.proxy.http",PROXY_HOST)
            fp.set_preference("network.proxy.http_port",int(PROXY_PORT))
            fp.set_preference("network.proxy.https",PROXY_HOST)
            fp.set_preference("network.proxy.https_port",int(PROXY_PORT))
            fp.set_preference("network.proxy.ssl",PROXY_HOST)
            fp.set_preference("network.proxy.ssl_port",int(PROXY_PORT))
            fp.set_preference("network.proxy.ftp",PROXY_HOST)
            fp.set_preference("network.proxy.ftp_port",int(PROXY_PORT))
            fp.set_preference("network.proxy.socks",PROXY_HOST)
            fp.set_preference("network.proxy.socks_port",int(PROXY_PORT))
            fp.update_preferences()
            return webdriver.Firefox(firefox_profile=fp)
        else:
            service_args = [
                '--proxy=' + PROXY_HOST + ':' + PROXY_PORT,
                '--proxy-type=http',
            ]
            return webdriver.PhantomJS(service_args=service_args)

    def login_to_fiverr(self):
        while not accounts_queue.empty():
            account = accounts_queue.get().strip().split(' ')
            email, password = account[0], account[1]
            self.browser.get(login_url)
            login_element = self.browser.find_element_by_class_name('js-form-login')
            login_element.send_keys(email)

            pass_el = self.browser.find_element_by_class_name('js-form-password')
            pass_el.send_keys(password)
            self.browser.find_element_by_id('login-btn').click()
            time.sleep(random.randrange(3, 5))
            if 'https://www.fiverr.com/' == self.browser.current_url:
                break
        if accounts_queue.empty():
            exit()

    def get_new_gigs(self):
        if (not urls_queue.empty() and self.last_page) or (not urls_queue.empty() and not self.working_url):
            self.working_url = urls_queue.get()
        elif urls_queue.empty() and self.last_page:
            exit()

        self.browser.get(self.working_url)
        time.sleep(random.randrange(3, 5))
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(random.randrange(1, 3))
        items = self.browser.find_elements_by_class_name('gig-item')

        for item in items:
            gig_id = item.get_attribute('data-gig-id')[48*self.page_number:]
            freelancer_name = item.get_attribute('data-gig-seller-name')[48*self.page_number:]
            self.gigs.append([gig_id, freelancer_name])

    def send_messages(self):
        time.sleep(random.randrange(2, 4))
        for gig in self.gigs:
            time.sleep(1)
            self.browser.get('https://www.fiverr.com/conversations/'
                             + gig[1] + '?related_gig_id=' + gig[0])
            textarea = self.browser.find_element_by_id('message_body')
            formatted_message = self.message.replace('{{ USERNAME }}', gig[1]).split('{{ NEWLINE }}')
            for x in formatted_message:
                textarea.send_keys(x)
                textarea.send_keys(Keys.ENTER)
            fl, created = Freelancer.create_or_get(freelancer_name=gig[1])
            if not created:
                time.sleep(2)
            else:
                # self.browser.find_element_by_name('commit').click()
                print('Message sent to:      ' + gig[1])
                fl.last_message_posted_time = datetime.datetime.now()
                fl.gig_id = int(gig[0])
                fl.message = self.database_mesage
                fl.save()
                time.sleep(random.randrange(self.timeout-1, self.timeout+1))

    def next_page(self):
        self.browser.get(self.working_url)
        time.sleep(3)
        try:
            load_more_div = self.browser.find_element_by_class_name('gig-load-more')
            load_more_div.find_element_by_tag_name('a').click()
        except Exception:
            pass
        self.page_number += 1
        time.sleep(3)
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(2)
        try:
            self.browser.find_element_by_class_name('mp-empty-search-results')
        except Exception:
            pass
        else:
            self.last_page = True

    def run(self):
        self.login_to_fiverr()
        while True:
            try:
                if not self.last_page:
                    self.get_new_gigs()
                    self.send_messages()
                    self.next_page()
                else:
                    self.get_new_gigs()
                    self.send_messages()
                    self.page_number = 0
                    self.last_page = False
            except Exception as e:
                print(e)
                exit()


def function_for_pool(data):
    msg = data[0]
    proxy = data[1]
    f = Fiverr(message=msg,
               proxy=proxy,
               timeout=message_timeout)
    f.run()


def prepare_data(quantity):
    result = []
    for x in range(quantity):
        result.append([messages[x].strip(), proxies[x].strip()])
    return result


if __name__ == "__main__":
    proxies_available = len(proxies)
    accounts_available = len(accounts)
    print('Proxies available: ' + str(proxies_available))
    print('Account available: ' + str(accounts_available))
    print('Target urls: ' + str(len(target_urls)))
    print('=======================================')
    processes_available = min(proxies_available, accounts_available)
    print('Maximum processes available: ' + str(processes_available))
    processes_available = int(input('Set processes: '))
    print('=======================================')
    message_timeout = int(input('Timeout beetwen messages: '))
    print('=======================================')
    driver_id = int(input('Driver 1 - Firefox or  2 - PhantomJS): '))

    pool = Pool(processes=processes_available)
    result = pool.map_async(function_for_pool, prepare_data(processes_available))
    result.get()

