from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import random


proxies = open('configurations/proxies.txt', 'r').readlines()

ten_minutes_url = 'https://10minutemail.com'
join_url = 'https://www.fiverr.com/join'


def generate_password():
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    upperalphabet = alphabet.upper()
    pw_len = 8
    pwlist = []

    for i in range(pw_len//3):
        pwlist.append(alphabet[random.randrange(len(alphabet))])
        pwlist.append(upperalphabet[random.randrange(len(upperalphabet))])
        pwlist.append(str(random.randrange(10)))
    for i in range(pw_len-len(pwlist)):
        pwlist.append(alphabet[random.randrange(len(alphabet))])

    random.shuffle(pwlist)
    return "".join(pwlist)


def run():
    while True:
        for x in range(len(proxies)):
            try:
                service_args = [
                    '--proxy=' + proxies[x],
                    '--proxy-type=http',
                ]
                browser = webdriver.PhantomJS(service_args=service_args)
                browser.get(ten_minutes_url)
                time.sleep(3)
                email = browser.find_element_by_id('mailAddress').get_attribute('value')
                browser.get(join_url)
                input_element = browser.find_element_by_class_name('js-form-join-section').\
                    find_element_by_tag_name('input')
                input_element.send_keys(email)
                input_element.send_keys(Keys.ENTER)
                time.sleep(3)
                name = email[:email.index('@')]
                browser.find_element_by_class_name('js-form-username').send_keys(name)
                time.sleep(2)
                password = generate_password()
                browser.find_element_by_class_name('js-form-password').send_keys(password)
                time.sleep(2)
                browser.find_element_by_id('join-btn').click()
                time.sleep(60)
                browser.get(ten_minutes_url)
                browser.find_element_by_id('ui-id-1').click()
                time.sleep(2)
                link = browser.find_element_by_xpath("//a[contains(text(), 'http://www.fiverr.com/linker?activation_token=')]").text
                browser.get(link)
                with open('configurations/accounts.txt', 'a') as file:
                    file.write(email + ' ' + password + '\n')
                browser.quit()
            except Exception:
                pass

run()